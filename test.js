const leak = require('./index.js');

const start = performance.now();
const leaked = leak(2**30);
const time = performance.now() - start;
if(leaked){
    console.log(`Leaked 1 GiB of memory in ${Math.round(time * 10) / 10} ms`);
}else{
    console.log('Failed to leak 1 GiB of memory');
}

setInterval(()=>{}, 1000); // Occupy event queue to the process doesn't exit
