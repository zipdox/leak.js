# leak.js

Native NodeJS library for leaking memory.

## Usage

`leak(bytes)`

Returns one of:
- `undefined` on error
- `true` if the memory was leaked successfully
- `false` if the memory couldn't be allocated

For example:

```javascript
const leak = require('leak.js');
leak(2 ** 30) // Leaks 1 GiB of memory
```
Leaking larger amounts of memory takes longer. If you want to leak a large amount of memory without blocking the main thread, I recommend using a worker thread.

## FAQ

### Why?
Because [pee.js](https://www.npmjs.com/package/pee.js) could be improved. It has a few shortcomings:
- Allocation is not optimized.
- Allocated memory is still accesible under `global.toilet`, so it isn't truly "leaked".
- It's not actually running `malloc()`, which is no fun.

leak.js is about 10-15% faster than pee.js at leaking memory, and makes leaked memory truly unfreeable.

### How can I free leaked memory?
Simply call `process.exit()`
