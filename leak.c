#include <node_api.h>
#include <stdlib.h>
#include <unistd.h>

napi_value leak_cb(napi_env env, napi_callback_info info){
    size_t argc = 1;
    napi_value amount_val;
    napi_get_cb_info(env, info, &argc, &amount_val, NULL, NULL);

    if(argc < 1) return NULL;

    int64_t leak_amount;
    if(napi_get_value_int64(env, amount_val, &leak_amount) != napi_ok) return NULL;
    if(leak_amount < 0) return NULL;
    
    unsigned char *leaked = malloc(leak_amount);
    if(leaked != NULL){
        int page_size = getpagesize();
        for(int64_t i = 0; i < leak_amount; i += page_size){
            leaked[i] = i & 0xFF;
        }
    }
    
    napi_value success;
    napi_get_boolean(env, leaked != NULL, &success);

    return success;
}


napi_value Init(napi_env env, napi_value exports){
    napi_value leak_fn;
    napi_create_function(env, NULL, NAPI_AUTO_LENGTH, leak_cb, NULL, &leak_fn);
    return leak_fn;
}

NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)
